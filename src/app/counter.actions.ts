import {Action} from '@ngrx/store';
import {ITodos} from './todos/todos.component';

export enum ActionTypes {
    Increment = '+',
    Decrement = '-',
    Reset = '0',
    Power = '^',
    AddArray = 'Add Array',
    AddArrayConfirmed = 'Add Array Confirmed',
    AddTodo = 'Add Todo',
    InsertTodo = 'Insert Todo',
    AppendTodo = 'Append Todo'
}

export class Increment implements Action {
    readonly type = ActionTypes.Increment;
}

export class Decrement implements Action {
    readonly type = ActionTypes.Decrement;
}

export class Power implements Action {
    readonly type = ActionTypes.Power;
}

export class AddArray implements Action {
    readonly type = ActionTypes.AddArray;
}

export class AddArrayConfirmed implements Action {
    readonly type = ActionTypes.AddArrayConfirmed;

    constructor(public payload: number) {}
}

export class AddTodo implements Action {
    readonly type = ActionTypes.AddTodo;

    constructor(public payload: ITodos) {}
}

export class InsertTodo implements Action {
    readonly type = ActionTypes.InsertTodo;

    constructor(public payload: ITodos) {}
}

export class AppendTodo implements Action {
  readonly type = ActionTypes.AppendTodo;

  constructor(public payload: ITodos) {}
}

export class Reset implements Action {
    readonly type = ActionTypes.Reset;
}
