import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';

import {StoreModule} from '@ngrx/store';
import {arrayReducer, counterReducer, todosReducer} from './counter.reduceer';
import {TestComponent} from './test/test.component';
import {environment} from '../environments/environment';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {TodosComponent} from './todos/todos.component';
import {EffectsModule} from '@ngrx/effects';
import {ArrayEffects} from './array/array.effects';
import {ReactiveFormsModule} from '@angular/forms';
import {ArrayComponent} from './array/array.component';
import {TodosEffects} from './todos/todos.effects';

@NgModule({
    declarations: [AppComponent, TestComponent, TodosComponent, ArrayComponent],
    imports: [
        BrowserModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature('TODOS_STORE', todosReducer),
        StoreModule.forFeature('ARRAY_STORE', arrayReducer),
        StoreModule.forFeature('COUNT_STORE', counterReducer),
        EffectsModule.forRoot([TodosEffects, ArrayEffects]),
        ReactiveFormsModule,
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
