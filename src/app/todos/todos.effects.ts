import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map} from 'rxjs/operators';

@Injectable()
export class TodosEffects {
    constructor(private actions$: Actions) {}

    @Effect()
    loadTodo$ = this.actions$.pipe(
        ofType('Add Todo'),
        map(add => {
            // @ts-ignore
            if (add.payload.insert) {
                // @ts-ignore
                return {type: 'Insert Todo', payload: add.payload};
            } else {
                // @ts-ignore
                return {type: 'Append Todo', payload: add.payload};
            }
        })
    );
}
