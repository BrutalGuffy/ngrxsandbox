import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {FormControl, FormGroup} from '@angular/forms';
import {AddTodo} from '../counter.actions';

export interface ITodos {
    todo: string;
    insert: boolean;
}

@Component({
    selector: 'app-todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./todos.component.css']
})
export class TodosComponent {
    todos$: Observable<ITodos>;

    todosForm = new FormGroup({
        todo: new FormControl(''),
        insert: new FormControl(false)
    });

    constructor(private store: Store<{todos: ITodos[]}>) {
        this.todos$ = store.pipe(select('TODOS_STORE'));
    }

    addTodo() {
        this.store.dispatch(
            new AddTodo({
                todo: this.todosForm.controls.todo.value,
                insert: this.todosForm.controls.insert.value
            })
        );
        this.todosForm.reset();
    }
}
