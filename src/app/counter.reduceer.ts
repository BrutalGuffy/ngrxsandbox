import {Action} from '@ngrx/store';
import {ActionTypes} from './counter.actions';
import {ITodos} from './todos/todos.component';

export const initialState = 0;

export const initialArray = [];

export const initialTodos: ITodos[] = [];

export function counterReducer(state = initialState, action: Action) {
    switch (action.type) {
        case ActionTypes.Increment:
            return state + 1;

        case ActionTypes.Decrement:
            return state - 1;

        case ActionTypes.Power:
            return state * 2;

        case ActionTypes.Reset:
            return 0;

        default:
            return state;
    }
}

export function arrayReducer(state = initialArray, action: any) {
    switch (action.type) {
        case ActionTypes.AddArrayConfirmed:
            return [...state, action.payload];

        default:
            return state;
    }
}

export function todosReducer(state = initialTodos, action: any) {
    switch (action.type) {
        case ActionTypes.AppendTodo:
            return [...state, action.payload];

        case ActionTypes.InsertTodo:
            return [action.payload, ...state];

        default:
            return state;
    }
}
