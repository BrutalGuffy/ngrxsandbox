import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AddArray} from '../counter.actions';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})
export class ArrayComponent implements OnInit {

  array$: Observable<number[]>;

  constructor(private store: Store<{array: number[]}>) {
    this.array$ = store.pipe(select('ARRAY_STORE'));
  }

  ngOnInit() {
    this.store.dispatch(new AddArray());
  }

}
