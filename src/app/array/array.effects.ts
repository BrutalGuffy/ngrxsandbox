import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';

@Injectable()
export class ArrayEffects {
    constructor(private actions$: Actions, private store: Store<any>) {}

    @Effect()
    loadTodo$ = this.actions$.pipe(
        ofType('Add Array'),
        switchMap(() =>
            this.store
                .pipe(select('COUNT_STORE'))
                .pipe(map(current => ({type: 'Add Array Confirmed', payload: current})))
        )
    );
}
