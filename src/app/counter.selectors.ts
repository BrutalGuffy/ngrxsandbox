import {createFeatureSelector, createSelector} from '@ngrx/store';

const featureSelector = createFeatureSelector<{count: number}>('COUNT_STORE');

export const getCurrent = createSelector(
    featureSelector,
    state => state.count
);
